# Yellotron Pro

## The project
An emulation of the Mellotron realised in Pure Data, from the original project by Pierre Massat.

Uploaded with all the sounds available [here](http://www.leisureland.us/mellotron.htm#)

## The sounds

These WAV samples were taken from 2 different Mellotrons...an M400S and an M4000. The tapes were in mint condition when sampled. The samples were recorded with the tone control fully clockwise. Each ZIP file contains one set of samples of about 20 megabytes. Each sample is approximately 7 to 8 seconds in length.

## Where did i find this patch?

[Here](https://guitarextended.wordpress.com/2014/11/11/yellotron-a-mellotron-with-pure-data/)!
